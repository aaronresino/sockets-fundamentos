const { io } = require('../server');

// Detecta la conexión de un usuario al socket.
io.on('connection', (client) => {
    console.log("User connected");

    client.emit('sendMessage', {
        user: "Admin",
        message: "Welcome to this app."
    });

    client.on('disconnect', () => {
        console.log("User disconnected");
    });

    // Listen client.
    client.on('sendMessage', (data, callback) => {
        console.log(data);

        client.broadcast.emit("sendMessage", data); // Con broadcast lo envía a todos los usuarios que estén conectados al servidor.

        // if (data.user) {
        //     callback({
        //         response: "Todo salió bien."
        //     });
        // } else {
        //     callback({
        //         response: "Salió mal."
        //     })
        // }
    });
});