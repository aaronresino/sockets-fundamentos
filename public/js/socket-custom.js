var socket = io();

// Inicia la conexión con el socket del backend.
socket.on("connect", function () {
    console.log("Conectado al servidor");
});

socket.on("disconnect", function () {
    console.log("Perdimos la conexión con el servidor");
});

// Enviar información del cliente al servidor.
socket.emit("sendMessage", {
    user: "Fernando",
    message: "Hola mundo del Socket en NodeJS."
}, function (response) {
    console.log("Server response:", response);
});

// Escuchar información del servidor.
socket.on("sendMessage", function (data) {
    console.log("Server:", data);
});